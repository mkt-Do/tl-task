package controllers

import javax.inject._
import play.api._
import play.api.libs.json.Json
import play.api.mvc._

@Singleton
class FrontController @Inject()(assets: Assets, cc: ControllerComponents) extends AbstractController(cc) {
  // return index.html from all path
  def index = assets.at(path = "/public", file = "index.html")
  def indexAll(path: String) = index

  // static
  def staticResource(resource: String) = assets.at(path = "/public/static", file = resource)
}
