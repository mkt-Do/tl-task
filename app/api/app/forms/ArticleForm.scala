package api.forms

import play.api.libs.json.Json
import play.api.libs.json.JsValue
import play.api.mvc._
import play.api.mvc.Results.BadRequest

case class ArticleForm(
  title: String,
  overview: String,
  content: String,
  isPrivate: Boolean
)

object ArticleForm {
  private def isEmptyStringValue(key: String, str: String): Either[Result, String] =
    if (str == "") Left(BadRequest(s"${key} should not be empty.")) else Right(str)

  def bindForm(json: JsValue): Either[Result, ArticleForm] = (for {
    title <- (json \ "title").asOpt[String].toRight(BadRequest("Missing parameter [title]")).right
    overview <- (json \ "overview").asOpt[String].toRight(BadRequest("Missing parameter [overview]")).right
    content <- (json \ "content").asOpt[String].toRight(BadRequest("Missing paramter [content]")).right
    isPrivate <- (json \ "is_private").asOpt[Boolean].toRight(BadRequest("Missing parameter [is_private]")).right
  } yield (title, overview, content, isPrivate)) match {
    case Left(e) => Left(e)
    case Right(p) => for {
      t <- isEmptyStringValue("title", p._1).right
      o <- isEmptyStringValue("overview", p._2).right
      c <- isEmptyStringValue("content", p._3).right
    } yield ArticleForm(t, o, c, p._4)
  }
}
