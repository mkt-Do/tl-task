package api.controllers

import javax.inject._
import play.api._
import play.api.libs.json.Json
import play.api.mvc._

import api.dao.UserDao
import api.dao.UserFollowRelationDao
import api.models.User

@Singleton
class UserApiController @Inject()(
  cc: ControllerComponents,
  userDao: UserDao,
  userFollowRelationDao: UserFollowRelationDao
) extends AbstractController(cc) {
  def info(displayId: String) = Action { implicit request: Request[AnyContent] =>
    userDao.findByDisplayId(displayId) match {
      case Some(user) => Ok(Json.toJson(user))
      case None => NotFound
    }
  }

  def follow(displayId: String) = Action { implicit request: Request[AnyContent] =>
    userDao.findByDisplayId(displayId) match {
      case Some(user) => {
        val follows = userFollowRelationDao.findFollowsByUser(user)
        Ok(Json.toJson(follows))
      }
      case None => NotFound
    }
  }
}
