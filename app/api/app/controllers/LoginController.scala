package api.controllers

import javax.inject._
import play.api._
import play.api.libs.json.Json
import play.api.mvc._

import api.dao.UserDao
import api.models.User

@Singleton
class LoginApiController @Inject()(
  cc: ControllerComponents,
  userDao: UserDao
) extends AbstractController(cc) {
  def login() = Action { implicit request: Request[AnyContent] =>
    request.body.asJson.map { json =>
      val user: Either[Result, User] = ((for {
        email <- (json \ "email").asOpt[String].toRight(BadRequest("Missing parameter [email]")).right
        password <- (json \ "password").asOpt[String].toRight(BadRequest("Missing parameter [password]")).right
      } yield (email, password)) match {
        case Left(e) => Left(e)
        case Right(p) => userDao.loginUser(p._1, p._2) match {
          case Left(e) => Left(e)
          case Right(u) => Right(u)
        }
      }) 
      user match {
        case Left(e) => e
        case Right(u) => Ok(Json.toJson(u))
      }
    }.getOrElse {
      BadRequest("Unexpected Request")
    }
  }
}
