package api.controllers

import java.time.OffsetDateTime
import javax.inject._
import play.api._
import play.api.libs.json.Json
import play.api.mvc._

import api.dao.ArticleDao
import api.dao.UserDao
import api.dao.UserFollowRelationDao
import api.forms.ArticleForm
import api.models.Article
import api.models.User

@Singleton
class ArticleApiController @Inject()(
  cc: ControllerComponents,
  articleDao: ArticleDao,
  userDao: UserDao,
  userFollowRelationDao: UserFollowRelationDao
) extends AbstractController(cc) {
  def info(displayId: String, id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(Json.obj("info" -> "info"))
  }

  def userArticles(displayId: String) = Action { implicit request: Request[AnyContent] =>
    val articles = articleDao.findByDisplayId(displayId)
    Ok(Json.toJson(articles))
  }

  def timelines(displayId: String) = Action { implicit request: Request[AnyContent] =>
    userDao.findByDisplayId(displayId) match {
      case Some(user) => {
        val follows = userFollowRelationDao.findFollowsByUser(user)
        val articles = articleDao.findByUserAndFollows(user, follows)
        Ok(Json.toJson(articles))
      }
      case None => NotFound
    }
  }

  def create(displayId: String) = Action { implicit request: Request[AnyContent] =>
    userDao.findByDisplayId(displayId) match {
      case Some(user) => {
        request.body.asJson.map { json =>
          ArticleForm.bindForm(json) match {
            case Left(e) => e
            case Right(form) => {
              articleDao.createArticle(form, user)
              Ok
            }
          }
        }.getOrElse {
          BadRequest("Unexpected Request")
        }
      }
      case None => NotFound
    }
  }
}
