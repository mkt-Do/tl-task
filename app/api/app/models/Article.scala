package api.models

import java.time.OffsetDateTime
import play.api.libs.json.Json
import scalikejdbc._

case class Article(
  id: Long,
  userId: Long,
  user: User,
  title: String,
  overview: String,
  content: String,
  isPrivate: Boolean = false,
  createdAt: OffsetDateTime,
  updatedAt: OffsetDateTime
)
object Article extends AbstractModel[Article] {

  override val schemaName: Option[String] = None
  override val tableName: String = "articles"

  implicit val jsonWrites = Json.writes[Article]

  def apply(a: ResultName[Article], u: ResultName[User])(rs: WrappedResultSet): Article = {
    val user = User(u)(rs)
    Article(
      rs.long(a.id),
      rs.long(a.userId),
      user,
      rs.string(a.title),
      rs.string(a.overview),
      rs.string(a.content),
      rs.boolean(a.isPrivate),
      rs.offsetDateTime(a.createdAt),
      rs.offsetDateTime(a.updatedAt)
    )
  }
}
