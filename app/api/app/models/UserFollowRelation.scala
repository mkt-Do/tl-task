package api.models

import java.time.OffsetDateTime
import scalikejdbc._

case class UserFollowRelation(
  id: Long,
  userId: Long,
  followId: Long,
  createdAt: OffsetDateTime,
  updatedAt: OffsetDateTime
)
object UserFollowRelation extends AbstractModel[UserFollowRelation] {
  
  override val schemaName: Option[String] = None
  override val tableName: String = "user_follow_relations"
}
