package api.models

import java.time.OffsetDateTime
import play.api.libs.json.{ Json, Writes }
import scalikejdbc._

case class User(
  id: Long,
  displayId: String,
  name: String,
  isProposer: Boolean = false,
  isCreator: Boolean = false,
  icon: String,
  profile: String,
  email: String,
  password: String,
  salt: String,
  lastLogined: OffsetDateTime,
  createdAt: OffsetDateTime,
  updatedAt: OffsetDateTime
)
object User extends AbstractModel[User] {

  override val schemaName: Option[String] = None
  override val tableName: String = "users"

  implicit val jsonWrites: Writes[User] = Writes { user =>
    Json.obj(
      "id" -> user.id,
      "display_id" -> user.displayId,
      "name" -> user.name,
      "is_proposer" -> user.isProposer,
      "is_creator" -> user.isCreator,
      "icon" -> user.icon,
      "profile" -> user.profile,
      "email" -> user.email
    )
  }

  def apply(u: ResultName[User])(rs: WrappedResultSet): User = {
    User(
      rs.long(u.id),
      rs.string(u.displayId),
      rs.string(u.name),
      rs.boolean(u.isProposer),
      rs.boolean(u.isCreator),
      rs.string(u.icon),
      rs.string(u.profile),
      rs.string(u.email),
      rs.string(u.password),
      rs.string(u.salt),
      rs.offsetDateTime(u.lastLogined),
      rs.offsetDateTime(u.createdAt),
      rs.offsetDateTime(u.updatedAt)
    )
  }
}
