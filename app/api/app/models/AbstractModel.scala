package api.models

import play.api.libs.json.{ JsonConfiguration, JsonNaming }
import scalikejdbc._

trait AbstractModel[T] extends SQLSyntaxSupport[T] {
  implicit val jsonConfiguration = JsonConfiguration(JsonNaming.SnakeCase)
}
