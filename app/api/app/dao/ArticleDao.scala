package api.dao

import java.time.OffsetDateTime
import javax.inject._
import scalikejdbc._

import api.forms.ArticleForm
import api.models.Article
import api.models.User
import api.models.UserFollowRelation

@Singleton
class ArticleDao {
  def createArticle(form: ArticleForm, user: User) = DB localTx { implicit session =>
    val a = Article.column
    withSQL {
      insert.into(Article)
        .columns(a.userId, a.title, a.overview, a.content, a.isPrivate, a.createdAt, a.updatedAt)
        .values(user.id, form.title, form.overview, form.content, form.isPrivate, OffsetDateTime.now, OffsetDateTime.now)
    }.update().apply()
  }

  def findByDisplayId(displayId: String) = DB readOnly { implicit session =>
    val (a, u) = (Article.syntax("a"), User.syntax("u"))
    withSQL {
      select.from(Article as a)
        .innerJoin(User as u)
        .on(a.userId, u.id)
        .where.eq(u.displayId, displayId)
        .orderBy(a.id).desc
    }.map(Article(a.resultName, u.resultName)(_)).list.apply()
  }

  def findByUserAndFollows(user: User, follows: Seq[User]) = DB readOnly { implicit session =>
    val ids = user.id +: follows.map(_.id)
    val (a, u) = (Article.syntax("a"), User.syntax("u"))
    withSQL {
      select.from(Article as a)
        .innerJoin(User as u)
        .on(a.userId, u.id)
        .where.in(a.userId, ids)
        .orderBy(a.id).desc
    }.map(Article(a.resultName, u.resultName)(_)).list.apply()
  }
}
