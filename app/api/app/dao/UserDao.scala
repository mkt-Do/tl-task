package api.dao

import java.time.OffsetDateTime
import javax.inject._
import scalikejdbc._
import play.api.mvc._
import play.api.mvc.Results.Unauthorized

import api.models.User

@Singleton
class UserDao {
  def findByDisplayId(displayId: String): Option[User] = DB readOnly { implicit session =>
    val u = User.syntax("u")
    withSQL {
      select.from(User as u)
        .where.eq(u.displayId, displayId)
    }.map(User(u.resultName)(_)).single.apply()
  }

  def loginUser(email: String, password: String): Either[Result, User] = DB readOnly { implicit session =>
    val u = User.syntax("u")
    withSQL {
      select.from(User as u)
        .where
        .eq(u.email, email)
        .and
        .eq(u.password, password)
    }.map(User(u.resultName)(_)).single.apply() match {
      case Some(u) => Right(u)
      case None => Left(Unauthorized)
    }
  }
}
