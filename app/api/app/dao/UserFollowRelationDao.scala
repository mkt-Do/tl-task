package api.dao

import java.time.OffsetDateTime
import javax.inject._
import scalikejdbc._

import api.models.User
import api.models.UserFollowRelation

@Singleton
class UserFollowRelationDao {
  def findFollowsByUser(user: User): Seq[User] = DB readOnly { implicit session =>
    val (ufr, u) = (UserFollowRelation.syntax("ufr"), User.syntax("u"))
    withSQL {
      select.from(UserFollowRelation as ufr)
        .innerJoin(User as u)
        .on(u.id, ufr.followId)
        .where.eq(ufr.userId, user.id)
    }.map(User(u.resultName)(_)).list.apply()
  }
}
