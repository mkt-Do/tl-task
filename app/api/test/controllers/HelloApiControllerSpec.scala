package api.controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._
import scala.concurrent.Future

class HelloApiControllerSpec extends PlaySpec with Results {
  "HelloApiController index" should {
    "test" in {
      val testController = new HelloApiController(stubControllerComponents())
      val indexRes = testController.index().apply(FakeRequest())
      val indexContent: String = contentAsString(indexRes)

      indexContent mustBe """{"hello":"world"}"""
    }
  }
}
