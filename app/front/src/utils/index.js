// @flow

const toLowerCamelCase = (str: string): string => {
  const s = str.charAt(0).toLowerCase() + str.slice(1);
  return s.replace(/[-_](.)/g, (match, group) => group.toUpperCase());
};

export const toLowerCamelCaseObject = (obj) => {
  return Object.entries(obj)
    .reduce((resultObj, [k, v]) => typeof v === 'object'
      ? ({ ...resultObj, [toLowerCamelCase(k)]: toLowerCamelCaseObject(v) })
      : ({ ...resultObj, [toLowerCamelCase(k)]: v }),
    {});
};
