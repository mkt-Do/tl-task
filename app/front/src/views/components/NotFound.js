import * as React from 'react';

const NotFound = () => {
  return (
    <div style={{textAlign: 'center'}}>
      <h1>404</h1>
      <h2>Not found</h2>
      <div>
        Please tell us about this page.
      </div>
    </div>
  );
};

export default NotFound;
