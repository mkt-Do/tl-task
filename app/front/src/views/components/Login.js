// @flow
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Input,
  Message,
  Segment,
} from 'semantic-ui-react';

class Login extends React.Component {

  get cookies() {
    return this.props.cookies;
  }

  get login() {
    return this.props.login;
  }

  render() {
    return (
      <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: '50vw' }}>
          <Header
            as='h2'
            color='teal'
            textAlign='center'
          >
            <Image src='/logo.png' /> Login
          </Header>
          <Form size='large'>
            <Segment stacked>
              <Form.Input
                content={ this.login.email }
                fluid
                icon='user'
                iconPosition='left'
                onChange={ (_, data) => this.props.setEmail(data.value) }
                placeholder='email'
              />
              <Form.Input
                content={ this.login.password }
                fluid
                icon='lock'
                iconPosition='left'
                onChange={ (_, data) => this.props.setPassword(data.value) }
                placeholder='password'
                type='password'
              />
              <Button
                color='teal'
                fluid size='large'
                onClick={ e => this.props.submitLogin(this.login) }
              >
                Login
              </Button>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

export default Login;
