import * as React from 'react';

const Foo = () => {
  return (
    <div>
      This is Foo.
    </div>
  );
};

export default Foo;
