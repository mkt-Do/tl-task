// @flow
import * as React from 'react';
import {
  Button,
  Checkbox,
  Form,
  Grid,
  Input,
  Segment,
  TextArea,
} from 'semantic-ui-react';

import SideBar from './SideBar';

class ArticleForm extends React.Component {

  componentDidMount() {
    const displayId = this.props.match.params.displayId;
    this.props.fetchUser(displayId);
  }

  get article() {
    return this.props.article;
  }

  render() {
    return(
      <Grid>
        <Grid.Column
          width={13}
        >
          <Form>
            {
              this.article.error !== null
                ? <Segment inverted color='red' tertiary>{ this.article.error }</Segment>
                : null
            }
            <Form.Field
              content={ this.article.title }
              control={ Input }
              label='title'
              onChange={ (_, data) => this.props.setTitle(data.value) }
              placeholder='title'
            />
            <Form.Field
              content={ this.article.overview }
              control={ Input }
              label='overview'
              onChange={ (_, data) => this.props.setOverview(data.value) }
              placeholder='This is your orticle overview...'
            />
            <Form.Field
              content={ this.article.content }
              control={ TextArea }
              label='content'
              onChange={ (_, data) => this.props.setContent(data.value) }
              placeholder='This is your article content...'
              rows={5}
            />
            <Form.Field
              checked={ this.article.isPrivate }
              control={ Checkbox }
              label='private'
              onChange={ (_, data) => this.props.setIsPrivate(data.checked) }
            />
            <Button
              floated='left'
              onClick={ e => this.props.submitArticle(this.props.match.params.displayId, this.article) }
              primary
            >
              SUBMIT
            </Button>
            <Button floated='right'>CANCEL</Button>
          </Form>
        </Grid.Column>
        <Grid.Column
          only='computer'
          width={3}
        >
          <SideBar
            disabled={false}
            match={ this.props.match }
            path={ this.props.location.pathname }
          />
        </Grid.Column>
      </Grid>
    );
  }
}

export default ArticleForm;
