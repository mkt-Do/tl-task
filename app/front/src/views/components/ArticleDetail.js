// @flow
import moment from 'moment';
import * as React from 'react';
import { Link } from 'react-router-dom';
import {
  Header,
  List,
  Segment,
} from 'semantic-ui-react';

const ArticleDetail = ({ article }) => {
  return (
    <Segment>
      <List divided relaxed>
        <List.Item>
          <Header as='h1'>{ article.title }</Header>
          <List.Description>{ moment(article.createdAt).format('YYYY-MM-DD') }</List.Description>
        </List.Item>
        <List.Item>
          <List.Header>Overview</List.Header>
          <List.Content>{ article.overview }</List.Content>
        </List.Item>
        <List.Item>
          <List.Header>Content</List.Header>
          <List.Content>{ article.content }</List.Content>
        </List.Item>
      </List>
    </Segment>
  )
};

export default ArticleDetail;
