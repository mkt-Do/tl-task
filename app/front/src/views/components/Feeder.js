// @flow
import moment from 'moment';
import * as React from 'react';
import { Link } from 'react-router-dom';
import {
  Feed,
  Icon,
  Item,
  Segment,
} from 'semantic-ui-react';

const Feeder = ({ articles }) => {
  return (
    <Segment>
      {
        articles.length === 0
          ? "See no articles."
          : articles.map(article =>
            <Feed>
              <Feed.Event>
                <Feed.Label
                  as={ Link }
                  content={ article.user.icon === ''
                    ? <Icon name='user' />
                    : <img src={ article.user.icon } />
                  }
                  to={ `/${article.user.displayId}/articles` }
                />
                <Feed.Content>
                  <Feed.Summary>
                    <Feed.User>{ article.user.name }</Feed.User>&nbsp;posted&nbsp;
                    <Link to={ `/${article.user.displayId}/articles/${article.id}` }>
                      { `${article.title}` }
                    </Link>
                    <Feed.Date>{ moment(article.createdAt).format('YYYY-MM-DD') }</Feed.Date>
                  </Feed.Summary>
                  <Feed.Extra>{ article.overview }</Feed.Extra>
                </Feed.Content>
              </Feed.Event>
            </Feed>
            )
      }
    </Segment>
  );
};

export default Feeder;
