// @flow
import * as React from 'react';
import { Card, Icon } from 'semantic-ui-react';

const UserCard = ({ user }) => {
  return (
    <Card
      description={ user.profile }
      extra={
        <a>
          <Icon name='user' />
          { user.follows.length } Friends
        </a>
      }
      header={ user.name }
      image={ user.icon }
      meta={ user.displayId }
    />
  );
};

export default UserCard;
