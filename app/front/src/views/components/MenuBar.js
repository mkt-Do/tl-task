// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Container,
  Dropdown,
  Image,
  Menu,
} from 'semantic-ui-react';

const dropdownMenus = [
  { label: 'logout', to: '/logout' },
];

const MenuBar = (props) => {
  return (
    <header>
      <Menu fixed='top' inverted>
        <Container>
          <Menu.Item as='a' header>
            <Image
              size='mini'
              src={ props.loginUser.icon }
              style={{ marginRight: '1.5em' }}
            />
          </Menu.Item>
          <Menu.Item as='a'><Link to={'/'}>Home</Link></Menu.Item>
          <Menu.Menu position='right'>
            <Dropdown item simple text='Accounts'>
              <Dropdown.Menu>
                {
                  dropdownMenus.map(e => {
                    return (
                      <Dropdown.Item
                        as={Link}
                        content={e.label}
                        to={e.to}
                      />
                    );
                  })
                }
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Container>
      </Menu>
    </header>
  );
};

export default MenuBar;
