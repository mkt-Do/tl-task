// @flow
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Item } from 'semantic-ui-react';

const UserItem = ({ user }) => {
  return (
    <Item>
      <Item.Image size='tiny' src={ user.icon } />
      <Item.Content>
        <Item.Header>{ user.name }</Item.Header>
        <Item.Meta>{ user.displayId }</Item.Meta>
        <Item.Description>{ user.profile }</Item.Description>
        <Item.Extra>
          {
            <a>
              <Icon name='user' />
              { user.follows.length } Friends
            </a>
          }
        </Item.Extra>
      </Item.Content>
    </Item>
  );
};

export default UserItem;
