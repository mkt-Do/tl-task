// @flow
import * as React from 'react';
import { Grid } from 'semantic-ui-react';

import ArticleDetail from './ArticleDetail';
import Feeder from './Feeder';
import NotFound from './NotFound';
import SideBar from './SideBar';
import UserCard from './UserCard';
import UserItem from './UserItem';

class Home extends React.Component {

  static getDerivedStateFromProps(nextProps) {
    const displayId = nextProps.match.params.displayId;
    if (displayId !== nextProps.user.displayId) {
      nextProps.fetchUser(displayId);
      nextProps.fetchArticles(displayId);
    }
    return;
  }
  
  get articles() {
    return this.props.user.articles;
  }

  get user() {
    return this.props.user;
  }

  render() {
    return (
      <Grid stackable>
        <Grid.Column
          only='computer tablet'
          width={4}
        >
          <UserCard user={ this.user } />
        </Grid.Column>
        <Grid.Column
          only='mobile'
          textAlign='center'
        >
          <UserItem user={ this.user } />
        </Grid.Column>
        <Grid.Column width={9}>
          {
            this.props.match.params.articleId
            ? this.articles.find(article => Number(this.props.match.params.articleId) === article.id) != null
              ? <ArticleDetail article={ this.articles.find(article => Number(this.props.match.params.articleId) === article.id) } />
              : <NotFound />
            : <Feeder
              articles={
                /^\/(.+)\/articles$/.test(this.props.location.pathname)
                  ? this.articles.filter(article => article.userId === this.user.id)
                  : this.articles
              }
            />
          }
        </Grid.Column>
        <Grid.Column
          only='computer'
          width={3}
        >
          <SideBar
            disabled={ this.props.match.params.displayId !== this.user.displayId }
            match={ this.props.match }
            path={ this.props.location.pathname }
          />
        </Grid.Column>
      </Grid>
    );
  }
}

export default Home;
