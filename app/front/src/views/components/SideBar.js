// @flow
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

const links = [
  { label: 'HOME', to: '' },
  { label: 'Your Articles', to: '/articles' },
  { label: 'Add Article', to: '/articles/new' },
  { label: 'to bar', to: '/too' },
];

const SideBar = ({ disabled, match, path }) => {
  return (
    <Menu fluid vertical tabular='right'>
      {
        links.map(e => {
          return (
            <Menu.Item
              active={ path === `/${match.params.displayId}${e.to}` }
              as={ Link }
              content={ e.label }
              disabled={ disabled }
              to={ `/${match.params.displayId}${e.to}` }
            />
          );
        })
      }
    </Menu>
  );
}

export default SideBar;
