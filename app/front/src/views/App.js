// @flow
import React from 'react';
import { Cookie, withCookies } from'react-cookie';
import { Redirect, Route, Switch } from 'react-router';
import { Container, Grid } from 'semantic-ui-react';

import ArticleForm from './containers/ArticleForm';
import Bar from './components/Bar';
import Foo from './components/Foo';
import Login from './containers/Login';
import NotFound from './components/NotFound';
import SideBar from './components/SideBar';
import Home from './containers/Home';
import MenuBar from './containers/MenuBar';

const data = [
  { label: 'to foo', path: '/foo' },
  { label: 'to bar', path: '/bar' },
];

class App extends React.Component {
  render() {
    return (
      <Container>
        <MenuBar data={data} cookies={ this.props.cookies } />
        <Container style={{ marginTop: '7em' }}>
          <Switch>
            <Route
              exact
              path={ '/' }
              render={ () => <Login cookies={ this.props.cookies }/> }
            />
            <Route exact path={ '/logout' } render={ () => <Redirect to={ '/' } /> } />
            <Route
              exact
              path={ '/:displayId' }
              component={ ({ location, match }) => <Home cookies={ this.props.cookies } location={ location } match={ match } /> }
            />
            <Route
              exact
              path={ '/:displayId/articles' }
              component={ ({ location, match }) => <Home cookies={ this.props.cookies } location={ location } match={ match } /> }
            />
            <Route
              exact
              path={ '/:displayId/articles/new' }
              component={ ({ location, match }) => <ArticleForm cookies={ this.props.cookies } location={ location } match={ match } /> } 
            />
            <Route
              exact
              path={ '/:displayId/articles/:articleId' }
              component={ ({ location, match }) => <Home cookies={ this.props.cookies } location={ location } match={ match } /> }
            />
            <Route component={ NotFound } />
          </Switch>
        </Container>
      </Container>
    );
  }
}

export default withCookies(App);
