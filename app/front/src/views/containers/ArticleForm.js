// @flow
import { connect } from 'react-redux';

import { articleOperations } from '../../state/ducks/article';
import { userOperations } from '../../state/ducks/user';
import ArticleForm from '../components/ArticleForm';

const mapStateToProps = (state, ownProps) => {
  return {
    article: state.articleState,
    cookie: ownProps.cookie,
    user: state.userState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchArticle: (articleId) => dispatch(articleOperations.fetchArticle(articleId)),
    fetchUser: (displayId) => dispatch(userOperations.fetchUser(displayId)),
    setArticleUser: (user) => dispatch(articleOperations.setArticleUser(user)),
    setContent: (content) => dispatch(articleOperations.setContent(content)),
    setIsPrivate: (isPrivate) => dispatch(articleOperations.setIsPrivate(isPrivate)),
    setOverview: (overview) => dispatch(articleOperations.setOverview(overview)),
    setTitle: (title) => dispatch(articleOperations.setTitle(title)),
    submitArticle: (displayId, article) => dispatch(articleOperations.submitArticle({ displayId, article })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleForm);
