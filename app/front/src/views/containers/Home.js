// @flow
import { connect } from 'react-redux';

import { articleOperations } from '../../state/ducks/article';
import { userOperations } from '../../state/ducks/user';
import Home from '../components/Home';

const mapStateToProps = (state, ownProps) => {
  return {
    cookies: ownProps.cookies,
    loginUser: state.loginState.loginUser,
    user: state.userState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchArticles: (displayId) => dispatch(articleOperations.fetchUserArticles(displayId)),
    fetchUser: (displayId) => dispatch(userOperations.fetchUser(displayId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
