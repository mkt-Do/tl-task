// @flow
import { connect } from 'react-redux';

import MenuBar from '../components/MenuBar';

const mapStateToProps = (state, ownProps) => {
  return {
    cookies: ownProps.cookies,
    loginUser: state.loginState.loginUser,
  };
};

export default connect(mapStateToProps)(MenuBar);
