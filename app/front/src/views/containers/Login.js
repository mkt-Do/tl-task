// @flow
import { connect } from 'react-redux';

import { loginOperations } from '../../state/ducks/login';
import Login from '../components/Login';

const mapStateToProps = (state, ownProps) => {
  return {
    cookies: ownProps.cookies,
    login: state.loginState,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setEmail: (email) => dispatch(loginOperations.setEmail(email)),
    setPassword: (password) => dispatch(loginOperations.setPassword(password)),
    submitLogin: (login) => dispatch(loginOperations.submitLogin(login)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
