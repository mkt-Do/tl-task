// @flow
import 'semantic-ui-css/semantic.min.css';

import { ConnectedRouter } from 'connected-react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import React from 'react';
import { CookiesProvider } from 'react-cookie';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';

import App from './views/App';
import rootSaga from './state/saga';
import * as serviceWorker from './serviceWorker';
import configureStore from './state/store';

const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const store = configureStore(
  window.REDUX_INITIAL_DATA,
  history,
  sagaMiddleware
);

sagaMiddleware.run(rootSaga);

const rootElement = document.getElementById('root');
if (rootElement) {
  ReactDOM.render(
    <CookiesProvider>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>
    </CookiesProvider>,
    rootElement
  );
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
