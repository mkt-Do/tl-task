// @flow
import { all } from 'redux-saga/effects';

import { articleSaga } from './ducks/article/operations';
import { loginSaga } from './ducks/login/operations';
import { userSaga } from './ducks/user/operations';

export default function* rootSaga() {
  yield all([
    ...articleSaga,
    ...loginSaga,
    ...userSaga,
  ]);
}
