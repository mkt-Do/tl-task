// @flow

const createInitialState = () => {
  return {
    form: {
      email: '',
      password: '',
    },
    loginUser: {
      id: 0,
      displayId: 0,
      name: '',
      isProposer: false,
      isCreator: false,
      icon: '',
      profile: '',
      email: '',
      articles: [],
      follows: [],
    },
  };
};

const setFormState = (state, form) => {
  return { ...state, form };
};

export default function loginReducer(
  state = createInitialState(),
  action,
) {
  switch (action.type) {
    case 'SET_EMAIL':
      return setFormState(state, { ...state.form, email: action.payload });
    case 'SEt_LOGIN_USER':
      return { ...state, user: action.payload };
    case 'SET_PASSWORD':
      return setFormState(state, { ...state.form, password: action.payload });
    default:
      return state;
  }
}
