// @flow
import type { User } from '../users/types';

export type Login = {
  form: {
    email: string,
    password: string,
  },
  loginUser: User,
};

export type Action =
  | { type: 'SET_EMAIL', payload: string }
  | { type: 'SET_LOGIN_USER', payload: User }
  | { type: 'SET_PASSWORD', payload: string }
  | { type: 'SUMBIT_LOGIN', payload: Login };
