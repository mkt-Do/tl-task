// @flow
import type { Action, Login } from './types';
import type { User } from '../user/types';

const setEmail = (email: string) => {
  return {
    type: 'SET_EMAIL',
    payload: email,
  };
};

const setLoginUser = (user) => {
  return {
    type: 'SET_LOGIN_USER',
    payload: user,
  };
};

const setPassword = (password: string) => {
  return {
    type: 'SET_PASSWORD',
    payload: password,
  };
};

const submitLogin = (login: Login) => {
  return {
    type: 'SUBMIT_LOGIN',
    payload: login,
  };
};

export default {
  setEmail,
  setLoginUser,
  setPassword,
  submitLogin,
};
