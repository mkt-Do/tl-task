// @flow
import { call, put, takeLatest } from 'redux-saga/effects';

import actions from './actions';
import type { Action, Login } from './types';
import { toLowerCamelCaseObject } from '../../../utils';

const postLogin = ({ email, password }) => {
  return fetch(`/api/login`,
    {
      body: JSON.stringify({
        email: email,
        password: password,
      }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
    },
  );
};

const setEmail = (email) => {
  return actions.setEmail(email);
};

const setLoginUser = (user) => {
  return actions.setLoginUser(user);
}

const setPassword = (password) => {
  return actions.setPassword(password);
};

const submitLogin = (login) => {
  return actions.submitLogin(login);
};

function* submitLoginToServer(action) {
  const res = yield call(postLogin, action.payload);
  if (res.ok) {
    const userJson = yield res.json();
    const user = toLowerCamelCaseObject(userJson);
    yield put(setLoginUser(user));
  } else {
    window.location = `/`;
  }
}

export const loginSaga = [
  takeLatest(
    'SUBMIT_LOGIN',
    submitLoginToServer,
  ),
];

export default {
  setEmail,
  setPassword,
  submitLogin,
};
