// @flow
import type { Action, Article } from './types';
import type { User } from '../user/types';

export const fetchUserArticles = (displayId: string): Action => {
  return { type: 'FETCH_USER_ARTICLES', payload: displayId };
};

export const setArticle = (article: Article): Action => {
  return { type: 'SET_ARTICLE', payload: article };
};

export const setContent = (content: string): Action => {
  return { type: 'SET_CONTENT', payload: content };
};

export const setError = (error: string): Action => {
  return { type: 'SET_ERROR', payload: error };
};

export const setIsPrivate = (isPrivate: boolean): Action => {
  return { type: 'SET_IS_PRIVATE', payload: isPrivate };
};

export const setOverview = (overview: string): Action => {
  return { type:'SET_OVERVIEW', payload: overview };
};

export const setTitle = (title: string): Action => {
  return { type: 'SET_TITLE', payload: title };
};

export const submitArticle = ({ displayId, article }): Action => {
  return { type: 'SUBMIT_ARTICLE', payload: { displayId, article } };
};

export default {
  fetchUserArticles,
  setArticle,
  setContent,
  setError,
  setIsPrivate,
  setOverview,
  setTitle,
  submitArticle,
};
