// @flow
import type moment from 'moment';

export type Article = {
  id: number,
  title: string,
  overview: string,
  content: string,
  isPrivate: boolean,
  createdAt: ?moment,
  updatedAt: ?moment,
  error: ?string,
};

export type Action =
  | { type: 'FETCH_USER_ARTICLES', payload: string }
  | { type: 'SET_ARTICLE', payload: Article }
  | { type: 'SET_CONTENT', payload: string }
  | { type: 'SET_ERROR', payload: string }
  | { type: 'SET_IS_PRIVATE', payload: boolean }
  | { type: 'SET_OVERVIEW', payload: string }
  | { type: 'SET_TITLE', payload: string }
  | { type: 'SUBMIT_ARTICLE', payload: { displayId: string, article: Article } };
