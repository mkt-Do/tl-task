// @flow
import type { Action, Article } from './types';

const createInitialState = () => {
  return {
    id: 0,
    title: '',
    overview: '',
    content: '',
    isPrivate: false,
    createdAt: null,
    updatedAt: null,
    error: null,
  };
};

const setState = (state, diff) => {
  return { ...state, ...diff };
};

export default function articleReducer(
  state = createInitialState(),
  action,
) {
  switch (action.type) {
    case 'SET_ARTICLE':
      return setState(state, action.payload);
    case 'SET_CONTENT':
      return setState(state, { content: action.payload });
    case 'SET_ERROR':
      return setState(state, { error: action.payload });
    case 'SET_IS_PRIVATE':
      return setState(state, { isPrivate: action.payload });
    case 'SET_OVERVIEW':
      return setState(state, { overview: action.payload });
    case 'SET_TITLE':
      return setState(state, { title: action.payload });
    default:
      return state;
  }
}
