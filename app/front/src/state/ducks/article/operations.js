// @flow
import { call, put, takeLatest } from 'redux-saga/effects';

import actions from './actions';
import userActions from '../user/actions';
import type { Action, Article } from './types';
import type { User } from '../user/types';
import { toLowerCamelCaseObject } from '../../../utils';

const setUserArticles = (articles: Array<Article>): Action => {
  return userActions.setArticles(articles);
};

const fetchUserArticles = (displayId: string): Action => {
  return actions.fetchUserArticles(displayId);
};

const getArticles = (displayId: string) => {
  return fetch(`/api/user/${displayId}/articles`)
    .then(res => res.json());
};

const postArticle = (displayId: string, article) => {
  return fetch(`/api/user/${displayId}/articles/new`,
    {
      body: JSON.stringify({
        title: article.title,
        overview: article.overview,
        content: article.content,
        is_private: article.isPrivate,
      }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
    },
  );
}

function* fetchUserArticlesFromServer(action) {
  const displayId = action.payload;
  const articles = yield call(getArticles, displayId);
  yield put(setUserArticles(
    articles.map(toLowerCamelCaseObject)
  ));
}

function* submitArticleFormToServer(action) {
  const { displayId, article } = action.payload;
  const res = yield call(postArticle, displayId, article);
  if (res.ok) {
    window.location = `/${displayId}`;
  } else {
    const error = yield res.text();
    yield put(setError(error));
  }
}

const setArticleUser = (user: User) => {
  return actions.setArticleUser(user);
};

const setContent = (content: string) => {
  return actions.setContent(content);
};

const setError = (error: string) => {
  return actions.setError(error);
};

const setIsPrivate = (isPrivate: boolean) => {
  return actions.setIsPrivate(isPrivate);
};

const submitArticle = ({displayId, article}) => {
  return actions.submitArticle({ displayId, article });
};

const setOverview = (overview: string) => {
  return actions.setOverview(overview);
};

const setTitle = (title: string) => {
  return actions.setTitle(title);
};

export const articleSaga = [
  takeLatest(
    'FETCH_USER_ARTICLES',
    fetchUserArticlesFromServer,
  ),
  takeLatest(
    'SUBMIT_ARTICLE',
    submitArticleFormToServer,
  )
];

export default {
  fetchUserArticles,
  setArticleUser,
  setContent,
  setIsPrivate,
  setOverview,
  setTitle,
  submitArticle,
};
