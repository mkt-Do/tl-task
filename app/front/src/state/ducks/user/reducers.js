// @flow
import type { Action, User } from './types';

const createInitialState = () => {
  return {
    id: 0,
    displayId: '',
    name: '',
    isProposer: false,
    isCreator: false,
    icon: '',
    profile: '',
    email: '',
    articles: [],
    follows: [],
  };
};

export default function userReducer(
  state = createInitialState(),
  action,
) {
  switch (action.type) {
    case 'SET_ARTICLES':
      return { ...state, articles: action.payload };
    case 'SET_FOLLOWS':
      return { ...state, follows: action.payload };
    case 'SET_USER':
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
