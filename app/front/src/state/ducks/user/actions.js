// @flow
import type { User, Action } from './types';

export const fetchUser = (displayId: string): Action => {
  return {
    type: 'FETCH_USER',
    payload: displayId,
  };
};

export const setArticles = (articles: Array<Article>): Action => {
  return {
    type: 'SET_ARTICLES',
    payload: articles,
  };
};

export const setFollows = (follows: Array<User>): Action => {
  return {
    type: 'SET_FOLLOWS',
    payload: follows,
  };
};

export const setUser = (user: User): Action => {
  return {
    type: 'SET_USER',
    payload: user,
  };
};

export default {
  fetchUser,
  setArticles,
  setFollows,
  setUser,
};
