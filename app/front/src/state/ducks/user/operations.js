// @flow
import { call, put, takeLatest } from 'redux-saga/effects';

import actions from './actions';
import type { Action, User } from './types';
import { toLowerCamelCaseObject } from '../../../utils';

const fetchUser = (displayId: string): Action => {
  return actions.fetchUser(displayId);
}

const setFollows = (follows: Array<User>): Action => {
  return actions.setFollows(follows);
};

const setUser = (user: User): Action => {
  return actions.setUser(user);
};

const getFollows = (displayId: string) => {
  return fetch(`/api/user/${displayId}/follows`)
    .then(res => res.json());
};

const getUser = (displayId: string) => {
  return fetch(`/api/user/${displayId}`)
    .then(res => res.json());
};

function* fetchUserFromServer(action) {
  const displayId = action.payload;
  const user = yield call(getUser, displayId);
  yield put(setUser(toLowerCamelCaseObject(user)));
  const follows = yield call(getFollows, displayId);
  yield put(setFollows(follows.map(toLowerCamelCaseObject)));
}

export const userSaga = [
  takeLatest(
    'FETCH_USER',
    fetchUserFromServer,
  ),
];

export default {
  fetchUser,
  setFollows,
  setUser,
};
