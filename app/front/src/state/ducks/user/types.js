// @flow
import type { Article } from '../article/types';

export type User = {
  id: number,
  displayId: string,
  name: string,
  isProposer: boolean,
  isCreator: boolean,
  icon: string,
  profile: string,
  email: string,
  articles: Array<Article>,
  follows: Array<User>,
};

export type Action =
  | { type: 'FETCH_USER', payload: string }
  | { type: 'SET_ARTICLES', payload: Array<Article> }
  | { type: 'SET_FOLLOWS', payload: Array<User> }
  | { type: 'SET_USER', payload: User };
