// @flow
import {
  connectRouter,
  routerMiddleware,
} from 'connected-react-router';
import {
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import * as reducers from './ducks';

export default function configureStore(
  initialState = {},
  history,
  sagaMiddleware
) {
  const rootReducer = combineReducers(reducers);

  return createStore(
    connectRouter(history)(rootReducer),
    composeWithDevTools(applyMiddleware(
      routerMiddleware(history),
      sagaMiddleware,
    )),
  );
}
