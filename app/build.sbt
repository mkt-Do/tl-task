name := """tl"""
organization := "com.mkt.do"

version := "1.0-SNAPSHOT"

lazy val api = (project in file("api")).enablePlugins(PlayScala)
lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .aggregate(api)
  .dependsOn(api)

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  jdbc,
  guice,
  "mysql" % "mysql-connector-java" % "8.0.13",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.scalikejdbc" %% "scalikejdbc" % "3.3.+",
  "org.scalikejdbc" %% "scalikejdbc-config" % "3.3.+",
  "org.scalikejdbc" %% "scalikejdbc-play-initializer" % "2.6.0-scalikejdbc-3.3"
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.mkt.do.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.mkt.do.binders._"
