-- temporary uncheck foreign key
SET FOREIGN_KEY_CHECKS = 0;

-- truncate
DROP TABLE IF EXISTS tl.articles;
DROP TABLE IF EXISTS tl.user_follow_relations;
DROP TABLE IF EXISTS tl.users;

-- check foreign key
SET FOREIGN_KEY_CHECKS = 1;

-- create table
CREATE TABLE tl.users (
  id int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  display_id varchar(255) NOT NULL UNIQUE,
  name varchar(255) NOT NULL,
  is_proposer tinyint(1) NOT NULL DEFAULT 0,
  is_creator tinyint(1) NOT NULL DEFAULT 0,
  icon varchar(255) DEFAULT NULL,
  profile text DEFAULT NULL,
  email varchar(255) NOT NULL UNIQUE,
  password varchar(255) NOT NULL,
  salt varchar(255) NOT NULL,
  last_logined datetime,
  created_at datetime DEFAULT CURRENT_TIMESTAMP,
  updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE tl.articles (
  id int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id int(10) unsigned NOT NULL,
  title varchar(255) NOT NULL,
  overview varchar(255) default '',
  content text NOT NULL,
  is_private tinyint(1) default 0,
  created_at datetime default CURRENT_TIMESTAMP,
  updated_at datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE tl.user_follow_relations (
  id int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_id int(10) unsigned NOT NULL,
  follow_id int(10) unsigned NOT NULL,
  created_at datetime default CURRENT_TIMESTAMP,
  updated_at datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE (user_id, follow_id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (follow_id) REFERENCES users(id)
);

-- insert record
INSERT INTO tl.users VALUES
  (1, 'mkt-Do', 'Makoto Ishizaki', 1, 1, '/static/images/pikachu.jpg', 'Engineer', 'mkt.koro.mikuru@gmail.com', '', '', now(), now(), now()),
  (2, 'follow-1', 'One Follow', 1, 0, '', 'Proposer', 'follow-one@sample.com', '', '', now(), now(), now()),
  (3, 'follow-2', 'Two Follow', 0, 1, '', 'Creator', 'follow-two@sample.com', '', '', now(), now(), now())
;

INSERT INTO tl.user_follow_relations VALUES
  (1, 1, 2, now(), now()),
  (2, 1, 3, now(), now()),
  (3, 2, 1, now(), now())
;

INSERT INTO tl.articles VALUES
  (1, 1, 'article title', 'This is overview of this article.', 'This is content of this article.', false, now(), now()),
  (2, 1, 'private article title', 'This is overview of this private article.', 'This is content of this private article.', true, now(), now()),
  (3, 2, 'follow-1 article', 'This is overview of follow-1 article', 'This is content of this follow-1 article', false, now(), now()),
  (4, 2, 'follow-1 private article', 'This is overview of follow-1 private article', 'This is content of this follow-1 private article', true, now(), now()),
  (5, 3, 'follow-2 article', 'This is overview of follow-2 article', 'This is content of this follow-2 article', false, now(), now()),
  (6, 3, 'follow-2 private article', 'This is overview of follow-2 private article', 'This is content of this follow-2 private article', true, now(), now())
;
