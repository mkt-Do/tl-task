FROM openjdk:8u151-jre-stretch

ENV PROJECT_NAME tl
ENV VERSION 1.0-SNAPSHOT
ENV SECRET mktdo

RUN mkdir /usr/src/app

WORKDIR /usr/src/app
COPY app/target/universal/${PROJECT_NAME}-${VERSION}.zip ./
RUN unzip ${PROJECT_NAME}-${VERSION}.zip

WORKDIR /usr/src/app/${PROJECT_NAME}-${VERSION}
RUN chmod a+x bin/${PROJECT_NAME}

CMD rm -rf RUNNING_PID && bin/tl -Dplay.http.secret.key=${SECRET}

EXPOSE 9000 9000
