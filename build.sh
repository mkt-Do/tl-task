#! /bin/bash

SCRIPT_DIR=`dirname $0`
IMAGE_NAME='tl'


cd $SCRIPT_DIR

# build scala
cd app
sbt dist

# build docker image
cd ../
docker image rm $IMAGE_NAME
docker build . -t $IMAGE_NAME
